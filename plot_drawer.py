import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from tkinter import *


class PlotDrawer:
    def __init__(self, title, window, tokens, xvals, yvals):
        self.window = window
        figure = plt.figure()
        figure.suptitle(title)
        self.axs = [figure.add_subplot(1, len(tokens), i+1) for i in range(len(tokens))]
        [self.axs[i].set_title('#' + tokens[i]) for i in range(len(tokens))]
        [ax.bar(xvals, yvals, align='center', color='blue') for ax in self.axs]
        [ax.set_xticks(xvals) for ax in self.axs]
        self.canvas = FigureCanvasTkAgg(figure, master=window)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)
        self.canvas._tkcanvas.pack(side=TOP, fill=BOTH, expand=1)

    def updateplot(self, queue):
        try:
            tid, results = queue.get()
            # print("QUEUE GET: ",tid,results)
            # self.axs[tid].cla()

            self.axs[tid].bar([i for i in range(0,24)], results, align='center', color='blue')

            self.canvas.draw()
            self.canvas.flush_events()

            self.window.after(1, self.updateplot, queue)
        except:
            self.window.after(1, self.updateplot, queue)

