import argparse
import multiprocessing
import time
import tweepy
from tweepy import OAuthHandler
from tkinter import *
from plot_drawer import PlotDrawer


def crawl(tid, token, queue, show):
    consumer_key = 'kQoBZkfm1B7wIdD3nRdht0k2x'
    consumer_secret = '0yozuFeGcU5cTkDa4nD9kO1BoBEaChU337il7qQ8NNhG5RJ4Pf'
    access_token = '993375396728012800-kNcHQdhFQVjjDW25ON38emToHWdMhF1'
    access_secret = '9KXgzZtzm36GTSPhXUNvZavKGijZ0djwMzrprC4jSHm07'

    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_secret)

    api = tweepy.API(auth)

    cursor = tweepy.Cursor(api.search, q=token, lang='en', include_entities=True,
                           tweet_mode="extended").items()

    data_array = [0 for i in range(24)]
    nr = 0

    while True:
        try:
            tweet = cursor.next()
            hour = tweet._json['created_at'][11:13]
            if show:
                print(100 * "=")
                print(nr, ")  ", token , "  tweeted at ", hour)
                print(tweet._json["full_text"])
                print(100 * "=")
            data_array[int(hour)] += 1
            nr += 1
            queue.put((tid, data_array))
        except tweepy.TweepError:
            print('Token: {}  - Sleeping...'.format(token))
            time.sleep(15 * 60 * 100)  # should be 15min
            continue
        except:
            break


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-t','--tokens', nargs='+', help='<Required> Tokens to search and show results.', required=True)
    parser.add_argument('-s','--show', action='store_true', help='<Optional> Show tweets.', required=False)
    args = parser.parse_args()

    window = Tk()
    queue = multiprocessing.SimpleQueue()

    tid = 0
    crawlers = []
    for token in args.tokens:
        crawlers.append(multiprocessing.Process(target=crawl, args=(tid, token, queue, args.show)))
        crawlers[tid].start()
        tid += 1

    plotdrawer = PlotDrawer("Hour of tweet", window, args.tokens, [i for i in range(24)], [0 for i in range(24)])
    plotdrawer.updateplot(queue)
    window.mainloop()